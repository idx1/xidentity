package org.idx.identity.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.idx.identity.common.UserManagerConstants;

import lombok.Data;

/**
 * Entity bean with JPA annotations
 * Hibernate provides JPA implementation
 *
 */
@Entity
@Table(name="USR")
@Data
public class User implements Serializable{
	@Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
      
	@Column(nullable = false)
    private String userName;
    private String password;
    private String email;
    private String firstName;  
    private String middleName; 
    private String lastName;
    private String passwordSalt;
    private String employeeId;
    private String createdBy;
    private String updatedBy;
    private Date createdDate;
    private Date updatedDate;
    
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="org_id")
	//@Column(nullable = false)
    private Organization organization; 
	
	

    @ManyToMany 
    @JoinTable(name="USR_GRP", 
          joinColumns=@JoinColumn(name="USR_ID"),
          inverseJoinColumns=@JoinColumn(name="GRP_ID"))
    private Collection<Group> groups;
	

    public User(){
    	groups = new ArrayList<Group>();
    	
    }

	public Collection<Group> getGroups() {
		return groups;
	}

	public void addGroups(Group group) {
        if (!getGroups().contains(group)) {
        	getGroups().add(group);
        }
        if (!group.getUsers().contains(this)) {
        	group.getUsers().add(this);
        }

	}

	
 }
