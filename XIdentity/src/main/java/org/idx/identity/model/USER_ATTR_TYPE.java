package org.idx.identity.model;

public enum USER_ATTR_TYPE {

	STRING (1),
	NUMBER (2),
	SECRET (3),
	BOOLEAN (4),
	DATE (5);
	private final int userAttrType;
	
	USER_ATTR_TYPE(int userAttrType){
		this.userAttrType=userAttrType;
	}

	public int getUserAttrType(){
		return userAttrType;
	}

}

