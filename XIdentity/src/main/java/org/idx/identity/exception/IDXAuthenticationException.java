package org.idx.identity.exception;

public class IDXAuthenticationException extends IDXException {

	public IDXAuthenticationException() {
		// TODO Auto-generated constructor stub
	}

	public IDXAuthenticationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public IDXAuthenticationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public IDXAuthenticationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public IDXAuthenticationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
