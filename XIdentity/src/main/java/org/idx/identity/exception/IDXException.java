package org.idx.identity.exception;

public class IDXException extends Exception {

	public IDXException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public IDXException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public IDXException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public IDXException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public IDXException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
