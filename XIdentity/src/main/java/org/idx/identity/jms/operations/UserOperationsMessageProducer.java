package org.idx.identity.jms.operations;

import org.idx.framework.jms.message.UserMessage;
import org.idx.identity.common.UserManagerConstants;
import org.idx.identity.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
 public class UserOperationsMessageProducer {
	
	  @Autowired 
	  private JmsTemplate jmsTemplate;
	  
	  public void postUserCreationMessage(User user) {
		  
		  UserMessage userMessage = new UserMessage(UserManagerConstants.USER_CREATE , user);
		  
		  System.out.println("Sending message");
		  
		  jmsTemplate.convertAndSend("UserOperationMessageQueue",userMessage);
		  
	  }

}
