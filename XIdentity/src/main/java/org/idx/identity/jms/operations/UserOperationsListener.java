package org.idx.identity.jms.operations;

import java.util.List;
import java.util.Map;

import org.idx.aggregator.dob.AggregationEventDOB;
import org.idx.aggregator.dob.AggregationUserMessage;
import org.idx.application.dob.MATCHING_ACTION;
import org.idx.application.dob.MATCHING_ACTION_CONDITION;
import org.idx.application.dob.MatchingActionDOB;
import org.idx.application.dob.MatchingRuleDOB;
import org.idx.identity.exception.UserManagerException;
import org.idx.identity.model.User;
import org.idx.identity.service.UserManagerService;
import org.idx.identity.utils.UserManagerUtils;
import org.springframework.beans.factory.annotation.Autowired;
//import org.idx.framework.jms.message.UserMessage;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class UserOperationsListener {
	
	@Autowired
	UserManagerService userManagerService;
	
//	  @JmsListener(destination = "UserOperationMessageQueue", containerFactory = "CPSFactory")
//	  public void receiveMessage(UserMessage userMessage) {
//
//		    System.out.println("Received <" + userMessage + ">");
//		    System.out.println("Received <" + userMessage.getOperation() + ">");
//		    System.out.println("Received <" + userMessage.getUser() + ">");
//	  }

//	  @JmsListener(destination = "AggregationOperationMessageQueue", containerFactory = "IDXFactory")
//	  public void receiveMessage(AggregationEventDOB aggEvent) {
//
//		    System.out.println("Received <" + aggEvent + ">");
//		    System.out.println("Received <" + aggEvent.getAttributes() + ">");
//		    System.out.println("Received <" + aggEvent.getEventState() + ">");
//	  }

	  @JmsListener(destination = "AggregationOperationMessageQueue", containerFactory = "IDXFactory")
	  public void receiveUserAggregationMessage(AggregationUserMessage userMessage) {

		    System.out.println("Received <" + userMessage + ">");
		    System.out.println("Received <" + userMessage.getMatchingActions() + ">");
		    System.out.println("Received <" + userMessage.getMatchingRule() + ">");
		    System.out.println("Received <" + userMessage.getUserAttributeMap() + ">");
		    
		    
		    List<MatchingActionDOB> matchingActions = userMessage.getMatchingActions();
		    MatchingRuleDOB matchingRule = userMessage.getMatchingRule();
		    Map<String, String> userAttrMap = userMessage.getUserAttributeMap();
		    
		    
		    String idxAttribute = matchingRule.getIdxAttribute();
		    String value = userAttrMap.get(idxAttribute);
		    
		    List<User> users = null;
			try {
				users = userManagerService.findUsers(idxAttribute, value);
			
		    
		    if(users.size() == 0) {
	 			for(MatchingActionDOB matchingAction : matchingActions) {
	 				
	 				MATCHING_ACTION aggAction = matchingAction.getAggAction();
	 				MATCHING_ACTION_CONDITION condition = matchingAction.getAggCondition();

	 				if(condition.equals(MATCHING_ACTION_CONDITION.NO_MATCH) && aggAction.equals(MATCHING_ACTION.CREATE))
						 
							userManagerService.createUser(userAttrMap);
						 
			    }
	 			
	 			

		    }
		    
			} catch (UserManagerException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
//		    System.out.println("Received <" + aggEvent.getAttributes() + ">");
//		    System.out.println("Received <" + aggEvent.getEventState() + ">");
	  }



}
