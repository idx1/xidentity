package org.idx.identity.dao;

import java.util.List;

import org.idx.framework.common.SearchCriteria;
import org.idx.identity.model.Organization;
import org.idx.identity.model.User;

public interface OrgManagerDAO {
	
	public Organization createOrg(Organization org);
	
	public Organization findOrgByName(String orgName);
	
	public Organization findOrgByID(long id);
	
	public List<Organization> findOrg(SearchCriteria criteria);



}
