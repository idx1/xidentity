package org.idx.identity.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.idx.framework.common.SearchCriteria;
import org.idx.framework.common.SearchCriteria.SEARCH_TYPE;
import org.idx.framework.common.SearchValue;
import org.idx.identity.common.UserSearchCriteria;
import org.idx.identity.dao.UserManagerDAO;
import org.idx.identity.exception.UserCreationException;
import org.idx.identity.exception.UserManagerException;
import org.idx.identity.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
@Transactional
public class UserManagerDAOImpl implements UserManagerDAO {

         
    @PersistenceContext
    private EntityManager manager;

	public List<User> findUser(SearchCriteria criteria) {
		
		List<User> list = new ArrayList<User>();
		
		Map<String, SearchValue> searchCriteria = criteria.getSearchCriteria();
		SEARCH_TYPE searchType = criteria.getSearchType();

		return list;
	}

	public User createUser(User user) throws UserManagerException{
		
		User savedUser = null;
		
		try {
		manager.persist(user);    	
//		logger.debug("Person saved successfully, Person Details="+user);
		
		Query query = manager.createQuery("Select u from User u where u.userName= :userName");
		query.setParameter("userName", user.getUserName());
		
		System.out.println("user.getUserName : "+ user.getUserName());
		
		savedUser = (User) query.getSingleResult();
		
		}catch(Exception e) {
			throw new UserManagerException(e.getMessage());
		}
		return savedUser;
	}

	public boolean checkIfUserLoginExists(String userLogin) {
		Query query = manager.createQuery("Select u from User u where u.userName= :userName");
		query.setParameter("userName", userLogin);
		
		List result = query.getResultList();
		
		if(result.size() > 0)
			return true;

		return false;
	}

	public void login(String userName, String password) {
		System.out.println("DAO userName : " + userName);
		System.out.println("DAO password : " + password);
	}

	public List<User> searchUser(UserSearchCriteria searchCriteria) {

		Query query = manager.createQuery("Select u from User u");
		
		List<User> users = query.getResultList();

		return users;
	}

	public List<User> findUser(String attributeName, String attributeValue) {
		
		System.out.println("attributeName : " + attributeName);
		System.out.println("attributeValue : " + attributeValue);


		Query query = manager.createQuery("Select u from User u where u."+ attributeName + "=:attributeValue");
		query.setParameter("attributeValue", attributeValue);

		List<User> users = query.getResultList();

		return users;
	}

 	 
}
