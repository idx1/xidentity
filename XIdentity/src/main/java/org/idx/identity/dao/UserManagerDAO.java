package org.idx.identity.dao;

import java.util.List;

import org.idx.framework.common.SearchCriteria;
import org.idx.identity.model.User;
import org.idx.identity.exception.UserCreationException;
import org.idx.identity.exception.UserManagerException;
import org.idx.identity.common.UserSearchCriteria;

  
public interface UserManagerDAO {

	public List<User> findUser(SearchCriteria criteria);

	public User createUser(User user) throws UserManagerException;

	public boolean checkIfUserLoginExists(String userLogin);

	public void login(String userName, String password);

	public List<User> searchUser(UserSearchCriteria searchCriteria);

	public List<User> findUser(String attributeName, String attributeValue);

}
