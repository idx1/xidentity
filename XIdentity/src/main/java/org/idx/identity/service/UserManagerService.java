package org.idx.identity.service;

import java.util.List;
import java.util.Map;

import org.idx.framework.common.SearchCriteria;
import org.idx.identity.common.UserSearchCriteria;
import org.idx.identity.exception.IDXAuthenticationException;
import org.idx.identity.exception.UserCreationException;
import org.idx.identity.exception.UserManagerException;
import org.idx.identity.model.User;
 
 
public interface UserManagerService {
	public List<User> findUser(SearchCriteria criteria);
	
	public User createUser(User user) throws UserManagerException;
	
	public boolean checkIfUserLoginExists(String userLogin);
	
	public void login(String userName, String password) throws IDXAuthenticationException;

	public List<User> searchUser(UserSearchCriteria searchCriteria);

	public List<User> findUser(String attributeName, String attributeValue);

	public void createUser(Map<String, String> userAttrMap) throws UserManagerException;
	
	public List<User> findUsers(String attributeName, String attributeValue) throws UserManagerException;
	
}
