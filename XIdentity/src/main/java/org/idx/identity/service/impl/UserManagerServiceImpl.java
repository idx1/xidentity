package org.idx.identity.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.idx.framework.common.IDXConstants;
import org.idx.framework.common.SearchCriteria;
import org.idx.identity.model.User;
import org.idx.identity.service.UserManagerService;
import org.idx.identity.utils.UserLoginGenerator;
import org.idx.identity.utils.UserManagerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.idx.identity.common.UserSearchCriteria;
import org.idx.identity.dao.UserManagerDAO;
import org.idx.identity.exception.IDXAuthenticationException;
import org.idx.identity.exception.UserCreationException;
import org.idx.identity.exception.UserManagerException;
import org.idx.identity.jms.operations.UserOperationsMessageProducer;
 
@Service
public class UserManagerServiceImpl implements UserManagerService{

	@Autowired
	private UserManagerDAO userManagerDAO;

	@Autowired
	UserOperationsMessageProducer userOperationsMessageProducer;

	@Autowired
	UserLoginGenerator userLoginGenerator;
		
	@Autowired
	UserManagerUtils userManagerUtils;

	
	public List<User> findUser(SearchCriteria criteria) {

		List<User> users = userManagerDAO.findUser(criteria);
		
		return users;
	}

	public User createUser(User user) throws UserManagerException{
		
		String userName = user.getUserName();
		
		System.out.println("USER KA NAAM : " + userName);

		if(userName != null) {
		
		boolean exists = userManagerDAO.checkIfUserLoginExists(userName);
		
		System.out.println("user : "+ user);
		 
		if(exists)
			throw new UserManagerException(IDXConstants.USER_ALREADY_EXISTS);
		
		}
		
		if(userName == null) {
			userName = generateUserLogin(user);
			user.setUserName(userName);
		}

		User usr = userManagerDAO.createUser(user);

		usr = userManagerUtils.performUserCreatePostProcessing(usr);
		
		return usr;
	}

	
	private String generateUserLogin(User user) throws UserManagerException {
		String userName = null;
		
		userName = userLoginGenerator.generateLogin(user);

		return userName;
	}

	private void performUserCreatePostProcessing(User user) {

		//Assign user to group
		assignUserToGroup(user);
		
		// Evaluate User Policy
		evaluateUserPolicy(user);
		
		// Do custom Post processing
		performCustomPostProcessingonCreate(user);
	}

	private void assignUserToGroup(User user) {
		// Add to CPS Group	
		
		// Get Users Org, based on org policy decide on people group
		
		// Get User Type, based on user type decide on users group
		
		 
	}

	private void evaluateUserPolicy(User user) {
		// Get Users group and provision application
		
	}

	private void performCustomPostProcessingonCreate(User user) {
		// TODO Auto-generated method stub
		
	}

	public boolean checkIfUserLoginExists(String userLogin) {
		return userManagerDAO.checkIfUserLoginExists(userLogin);

	}

	public void login(String userName, String password) throws IDXAuthenticationException {
		userManagerDAO.login(userName, password);		
	}

	public List<User> searchUser(UserSearchCriteria searchCriteria) {

		return userManagerDAO.searchUser(searchCriteria);
	}

	public List<User> findUser(String attributeName, String attributeValue) {
	    System.out.println("findUsers <" + attributeName + ">");
	    System.out.println("findUsers <" + attributeValue + ">");

		return userManagerDAO.findUser(attributeName, attributeValue);
	}

	public void createUser(Map<String, String> userAttrMap) throws UserManagerException {

		System.out.println("createUser : "+ userAttrMap);
		
		Set<String> keys= userAttrMap.keySet();
		
		User user = new User();
		
		for(String key:keys) {
			
			System.out.println("key : "+ key);

			if(key.equals("firstName")) {
				user.setFirstName(userAttrMap.get(key));
			}

			if(key.equals("lastName")) {
				user.setLastName(userAttrMap.get(key));
			}
			if(key.equals("middleName")) {
				user.setMiddleName(userAttrMap.get(key));
			}
			if(key.equals("email")) {
				user.setEmail(userAttrMap.get(key));
			}
			if(key.equals("employeeId")) {
				user.setEmployeeId(userAttrMap.get(key));
			}
			System.out.println("createUser : "+ user);

			
		}
		
		createUser(user);

	}
	
	public List<User> findUsers(String attributeName, String attributeValue) throws UserManagerException{
	    System.out.println("findUsers <" + attributeName + ">");
	    System.out.println("findUsers <" + attributeValue + ">");

		List<User> users = userManagerDAO.findUser(attributeName, attributeValue);

		return users;
	}

 
 
}
