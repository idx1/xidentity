package org.idx.identity.controller;

import java.util.Date;

import org.idx.identity.exception.UserCreationException;
import org.idx.identity.exception.UserManagerException;
import org.idx.identity.model.User;
import org.idx.identity.service.UserManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user/")
public class UserManagerController {

	@Autowired
	@Lazy
	UserManagerService userManager;
	
	@RequestMapping("createUser")	
	public ServerResponse crerateUser(@RequestParam("userObj") User user){
		
		try {
			userManager.createUser(user);
		} catch (UserManagerException e) {
			e.printStackTrace();
			return getServerResponse(ServerResponseCode.ERROR, true);
		}
		
		return getServerResponse(ServerResponseCode.SUCCESS, true);
	}
	
	public ServerResponse getServerResponse(int responseCode, Object data){
		ServerResponse serverResponse = new ServerResponse();
		serverResponse.setStatusCode(responseCode);
		serverResponse.setData(data);
		return serverResponse; 
	}

}
